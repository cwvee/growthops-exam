import "./App.css";
import AppNavbar from "./components/AppNavbar";

import Users from "./pages/Users";
import { Fragment, useState } from "react";
import { Container, Dropdown, Form, Button } from "react-bootstrap";
import { UserProvider } from "./UserContext";

function App() {
  const [date, setDate] = useState("");
  const [users, setUsers] = useState([]);
  const [select, setSelect] = useState("");

  function countMe() {
    for (let i = 1; i < 101; i++) {
      if (i % 3 === 0) {
        console.log("Foo");
      } else if (i % 5 === 0) {
        console.log("Bar");
      } else {
        console.log(i);
      }
    }
  }
  countMe();

  function convert() {
    let convertedDate = date.split("/").reverse().join("-");
    alert(`Converted date format: ${convertedDate}`);
  }

  function categoryChange(e) {
    setSelect(e.target.value);
  }

  return (
    <UserProvider value={{ users, setUsers, select, setSelect }}>
      <Fragment>
        <AppNavbar />
        <h1 className="text-center py-4">direct manager</h1>
        <Container>
          <p className="mt-5 mb-2">Filter by Age</p>
          <div>
            <div className="select-filter">
              <select className="filter py-2" onChange={categoryChange}>
                <option value="">All</option>
                <option value="20below">20 and below</option>
                <option value="21-39">21 to 39</option>
                <option value="40above">40 and above</option>
              </select>
            </div>
          </div>
          <hr />
          <Users />
          <hr />
          <Form onSubmit={() => convert()} className="my-5">
            <Form.Label>Date</Form.Label>
            <div className="d-flex">
              <Form.Group className=" w-25" controlId="date">
                <Form.Control
                  className="bg-white"
                  type="text"
                  placeholder="DD/MM/YYYY"
                  value={date}
                  onChange={(e) => setDate(e.target.value)}
                />
              </Form.Group>

              <Button type="submit" id="btn-custom" className="mx-3">
                convert
              </Button>
            </div>
          </Form>
        </Container>
      </Fragment>
    </UserProvider>
  );
}

export default App;
