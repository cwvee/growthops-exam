import { Fragment, useContext, useEffect } from "react";
import { Row } from "react-bootstrap";
import UserCard from "../components/UserCard";
import UserContext from "../UserContext";

export default function Users() {
  const { users, setUsers, select } = useContext(UserContext);
  useEffect(() => {
    fetch("http://www.mocky.io/v2/5d73bf3d3300003733081869")
      .then((res) => res.json())
      .then((data) => {
        if (select === "") {
          setUsers(
            data.map((user) => {
              return <UserCard key={user.id} userProp={user} />;
            })
          );
        } else if (select === "20below") {
          setUsers(
            data.map((user) => {
              if (user.age <= 20) {
                return <UserCard key={user.id} userProp={user} />;
              }
            })
          );
        } else if (select === "21-39") {
          setUsers(
            data.map((user) => {
              if (user.age <= 39 && user.age >= 21) {
                return <UserCard key={user.id} userProp={user} />;
              }
            })
          );
        } else if (select === "40above") {
          setUsers(
            data.map((user) => {
              if (user.age >= 40) {
                return <UserCard key={user.id} userProp={user} />;
              }
            })
          );
        }
      });
  }, [select]);

  return (
    <Fragment>
      <Row xs={1} md={2} lg={3}>
        {users}
      </Row>
    </Fragment>
  );
}
