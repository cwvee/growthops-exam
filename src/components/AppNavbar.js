import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Container from "react-bootstrap/Container";
import Logo from "../img/logo.png";
import "../App.css";

export default function AppNavbar() {
  return (
    <Navbar className="bg-color" expand="lg">
      <Container fluid>
        <Navbar.Brand href="#home">
          <img
            src={Logo}
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt="logo"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto nav text-white mx-5">
            <Nav.Link className="nav-item text-white" href="#home">
              Our Products
            </Nav.Link>
            <Nav.Link className="nav-item text-white" href="#link">
              About Us
            </Nav.Link>
            <Nav.Link className="nav-item text-white" href="#link">
              Live Better
            </Nav.Link>
            <Nav.Link className="nav-item text-white" href="#link">
              Claims &amp; Support
            </Nav.Link>
            <Nav.Link className="nav-item text-white" href="#link">
              My Account
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
