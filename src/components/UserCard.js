import { Card, Col } from "react-bootstrap";
import Image from "../img/userphoto.png";

export default function UserCard({ userProp }) {
  const { name, email, phone, age, address, website, company } = userProp;

  return (
    <Col>
      <Card className="mt-4 user-card">
        <Card.Body className="d-flex m-4 card-body">
          <div>
            <img
              src={Image}
              width="112"
              height="112"
              className="d-inline-block align-top"
              alt="user"
            />
          </div>
          <div>
            <Card.Title className="name">
              <strong>{name}</strong>
            </Card.Title>
            <Card.Text className="mb-1">
              Email: <strong>{email}</strong>
            </Card.Text>
            <Card.Text className="mb-1">
              Mobile: <strong>{phone}</strong>
            </Card.Text>
            <Card.Text className="mb-1">
              Company: <strong>{company}</strong>
            </Card.Text>
            <Card.Text className="mb-1">
              Address: <strong>{address.street}</strong>
            </Card.Text>
            <Card.Text className="mb-1">
              Website: <strong>{website}</strong>
            </Card.Text>
            <Card.Text className="mb-1">
              Age: <strong>{age}</strong>
            </Card.Text>
          </div>
        </Card.Body>
      </Card>
    </Col>
  );
}
